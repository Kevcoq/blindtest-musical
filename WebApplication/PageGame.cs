﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace WebApplication
{
    public class PageGame : System.Web.UI.Page
    {
        // Répertoire des mp3
        protected static string dirName;
        // Liste des fichiers
        protected static List<string> filesNames = new List<string>();

        // jeux
        public enum Statut { Login, Create, Wait, Play }
        protected static Statut statut;
        public int partyId
        {
            get { object o = ViewState["partyId"]; return (o == null) ? -1 : (int)o; }
            set { ViewState["partyId"] = value; }
        }

        public int userId
        {
            get { object o = ViewState["userId"]; return (o == null) ? -1 : (int)o; }
            set { ViewState["userId"] = value; }
        }

        // ####################################################
        // #####################    INIT    ###################
        // ####################################################
        /// <summary>
        /// Executer au démarrage du serveur
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void loadPage()
        {
            if (filesNames.Count == 0)
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                dirName = Path.GetDirectoryName(path) + @"\..\music\";


                /* Ajout des fichiers mp3 au tableau de nom. */
                DirectoryInfo fileListing = new DirectoryInfo(dirName);
                foreach (FileInfo file in fileListing.GetFiles("*.mp3"))
                    filesNames.Add(file.Name.Substring(0, file.Name.Length - 4));
            }
        }
    }
}

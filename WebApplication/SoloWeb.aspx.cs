﻿using GameLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Media;
using System.Reflection;
using System.Web.UI.WebControls;
using System.Threading;
using WcfGame;


namespace WebApplication
{
    public partial class SoloWeb : PageGame
    {
        // jeux
        private SoloWcfGame wcfGame;









        // ####################################################
        // ##############    VISIBILITE PANEL    ##############
        // ####################################################

        /// <summary>
        /// Change la visibilité des pannels
        /// </summary>
        private void switchVisibility()
        {
            switchStatut();
            exit.Visible = false; panLogin.Visible = false; panLogout.Visible = false; panPartyCreate.Visible = false; panScores.Visible = false; panReponses.Visible = false;
            switch (statut)
            {
                case Statut.Login: panLogin.Visible = true; break;
                case Statut.Create: panLogout.Visible = true; panPartyCreate.Visible = true; break;
                case Statut.Play: display(); panReponses.Visible = true; panLogout.Visible = true; panScores.Visible = true; exit.Visible = true; break;
            }
            loadPlayer();
            this.Validate();
        }

        /// <summary>
        /// Change le statut en fonction de l'environnement
        /// </summary>
        private void switchStatut()
        {
            if (userId == -1) statut = Statut.Login;
            else if (partyId == -1) statut = Statut.Create;
            else statut = Statut.Play;
        }



        /// <summary>
        /// Charges les joueurs en ligne
        /// </summary>
        private void loadPlayer()
        {
            // Maj stats
            if (userId != -1)
            {
                Player player = wcfGame.player(userId);
                scoresTotal.Text = player.scores.ToString();
                pourcentageTotal.Text = player.percentage().ToString();
            }
            this.Validate();
        }








        // ####################################################
        // #####################    INIT    ###################
        // ####################################################
        /// <summary>
        /// Executer au chargement de la page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            loadPage();
            wcfGame = SoloWcfGame.getInstance();
            wcfGame.setNbMusic(filesNames.Count);
            switchVisibility();
        }










        // ####################################################
        // ##############    LOGIN / LOGOUT    ################
        // ####################################################
        /// <summary>
        /// Connexion de l'utilisateur
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void loginClick(object sender, EventArgs e)
        {
            userId = wcfGame.login(inPseudo.Value, inPassword.Value);
            if (userId != -1) { switchVisibility(); }
            else Response.Write(@"<script language='javascript'>alert('Mot de passe incorrecte');</script>");
        }


        /// <summary>
        /// Deconnexion d'un utilisateur
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void logoutClick(object sender, EventArgs e)
        {
            wcfGame.disconnect(userId, partyId);
            partyId = -1;
            userId = -1;
            switchVisibility();
        }



        // ####################################################
        // ####################    PARTY    ###################
        // ####################################################
        /// <summary>
        /// Créer une partie
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void createClick(object sender, EventArgs e)
        {
            partyId = wcfGame.createParty(userId);
            wcfGame.init(partyId, listLevel.Value);
            generateTurn();
            switchVisibility();
        }



        /// <summary>
        /// Quitte la partie et ramene au menu de selection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void exitPartyClick(object sender, EventArgs e)
        {
            partyId = -1;
            responseRadioButton.Items.Clear();
            wcfGame.resetPlayer(userId);
            switchVisibility();
        }



        // ####################################################
        // #####################    GAME    ###################
        // ####################################################
        /// <summary>
        /// Gere l'affichage
        /// </summary>
        private void display()
        {
            Player player = wcfGame.player(userId);
            scoreValue.Text = player.tmpGood.ToString();
            essaiValue.Text = player.tmpTest.ToString();
            this.Validate();
        }

        /// <summary>
        /// Affiche les choix possible
        /// </summary>
        private void writeChoice()
        {
            IGame game = wcfGame.game(partyId);
            int[] result = game.generateChoice(filesNames.Count);
            responseRadioButton.Items.Clear();

            for (int i = 0; i < result.Length; i++)
                responseRadioButton.Items.Add(filesNames[result[i]]);
            this.Validate();
        }

        /// <summary>
        /// Genere un tour
        /// </summary>
        private void generateTurn()
        {
            /* Affichage */
            display();
            writeChoice();

            /* Lecture. */
            musicPlayer.Src = "/music/" + filesNames[wcfGame.game(partyId).getMusicPlayed()] + ".mp3";
        }

        /// <summary>
        /// Joue un tour.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void response_CheckedChanged(object sender, EventArgs e)
        {
            wcfGame.play(partyId, responseRadioButton.SelectedIndex);
            generateTurn();
        }
    }
}
﻿<%@ Page Title="Blind Test Musical" Language="C#" AutoEventWireup="true" CodeBehind="SoloWeb.aspx.cs" Inherits="WebApplication.SoloWeb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>

    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />


    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet" />

    <!-- Custom CSS -->
    <link href="css/business-casual.css" rel="stylesheet" />

    <!-- Fonts -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css" />
    <link href="http://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <form id="form1" runat="server">

        <div class="brand">Blind Test Musical</div>
        <div class="address-bar">Kevin Coquart | UPMC</div>


        <div class="container">
            <div class="row">
                <div class="box">





                    <%-- Login d'un utilisateur (pseudo, password) --%>
                    <asp:Panel ID="panLogin" runat="server">
                        <div class="col-lg-12 text-center">
                            <h2>Connexion</h2>
                            <div class="responseCenter">
                                <fieldset>
                                    <input runat="server" id="inPseudo" type="text" placeholder="Pseudo" required="required" />
                                    <input runat="server" id="inPassword" type="password" placeholder="Mot de passe" required="required" />
                                    <button runat="server" type="button" class="btn btn-primary" onserverclick="loginClick">Connexion</button>
                                </fieldset>
                            </div>
                        </div>
                    </asp:Panel>








                    <%-- Création d'une partie (Niveau, NombreJouerMax, NomPartie) et partie disponible --%>
                    <asp:Panel ID="panPartyCreate" runat="server">
                        <%-- Creation --%>
                        <div class="col-lg-10 text-center">
                            <h2>Créer une partie</h2>
                            <div class="responseCenter">
                                <fieldset>
                                    <input runat="server" id="listLevel" list="levelList" placeholder="Niveau" required="required" />
                                    <datalist id="levelList">
                                        <option value="Facile" />
                                        <option value="Moyen" />
                                        <option value="Difficile" />
                                        <option value="Extreme" />
                                    </datalist>

                                    <button runat="server" type="button" onserverclick="createClick" class="btn btn-primary">Créer</button>
                                </fieldset>
                            </div>
                        </div>
                    </asp:Panel>






                    <%-- Réponse au quizz --%>
                    <asp:Panel ID="panReponses" runat="server">
                        <div class="col-lg-10 text-center">
                            <h2 id="titleResponse" runat="server">Réponses :</h2>
                            <div id="divResponse" class="responseCenter" runat="server">
                                <asp:RadioButtonList ID="responseRadioButton" class="responseCenter" runat="server" AutoPostBack="True" OnSelectedIndexChanged="response_CheckedChanged">
                                </asp:RadioButtonList>

                                <audio id="musicPlayer" runat="server" src="C:\Users\Kevin\Documents\Visual Studio 2013\Projects\BlindTest Music\WebApplication\bin\music\Wizard.mp3" autoplay="autoplay" controls="controls">
                                    Your browser does not support the audio tag.</audio>
                            </div>
                        </div>
                    </asp:Panel>






                    <%-- Deconnexion / statistique --%>
                    <asp:Panel ID="panLogout" runat="server">
                        <div class="col-lg-2 text-center">
                            <h3>Stats</h3>
                            <div class="responseCenter">
                                Scores :
                                <asp:Label ID="scoresTotal" runat="server" Text=""></asp:Label><br />
                                Pourcentage :
                                <asp:Label ID="pourcentageTotal" runat="server" Text=""></asp:Label>
                                %<br />
                                <button runat="server" class="btn btn-primary" type="button" onserverclick="logoutClick">Deconnexion</button><br />
                                <button id="exit" runat="server" class="btn btn-primary" type="button" onserverclick="exitPartyClick">Quitter</button>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>









            <%-- Score de la partie courante --%>
            <asp:Panel ID="panScores" runat="server">
                <div class="row">
                    <div class="box">
                        <%-- Son score --%>
                        <div class="col-lg-12">
                            <hr />
                            <h2 class="intro-text text-center">Blind Test Musical : 
                        <strong>Score</strong>
                            </h2>
                            <hr />
                            <p class="scoreCenter">
                                Score : 
                <asp:Label ID="scoreValue" runat="server" Text="0"></asp:Label>
                                /<asp:Label ID="essaiValue" runat="server" Text="0"></asp:Label>
                            </p>
                        </div>
                    </div>
                </div>
            </asp:Panel>







            <%-- Notice --%>
            <div class="row">
                <div class="box">
                    <div class="col-lg-12">
                        <hr />
                        <h2 class="intro-text text-center">Blind Test Musical : 
                        <strong>Notice</strong>
                        </h2>
                        <hr />
                        <img class="img-responsive img-border img-left" src="img/intro-pic.jpg" alt="" />
                        <hr class="visible-xs" />
                        <p>Choisir son niveau de difficulté, cliquer sur la réponse souhaitée. Les statistiques sont actualisé à chaque tour.</p>
                        <p>Have fun !!!</p>
                    </div>
                </div>
            </div>






            <%-- Réalisation --%>
            <div class="row">
                <div class="box">
                    <div class="col-lg-12">
                        <hr />
                        <h2 class="intro-text text-center">Blind Test Musical : 
                        <strong>Réalisation</strong>
                        </h2>
                        <hr />
                        <p>
                            Le serveur est réalisé en .Net WCF, le mode API permet de porter son utilisation sur différentes plateformes.<br />
                            Une bibliothéque est utilisé pour partager le maximum de code entre la version locale et la version en ligne.
                        </p>
                        <p>Le site est réalisé via bootStrap pour permettre un rendu visuel correcte et éviter la perte de temps sur du code html.</p>
                        <p>Les comptes utilisateurs sont sauvegardés dans une base de donnée via Entity Framework.</p>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.container -->






        <%-- Copyright --%>
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <p>Copyright &copy; Kevin Coquart 2014</p>
                    </div>
                </div>
            </div>
        </footer>






        <!-- jQuery Version 1.11.0 -->
        <script src="js/jquery-1.11.0.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
    </form>
</body>
</html>

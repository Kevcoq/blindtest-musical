﻿using GameLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Media;
using System.Reflection;
using System.Web.UI.WebControls;
using System.Threading;
using WcfGame;


namespace WebApplication
{
    public partial class MultiWeb : PageGame
    {
        // jeux
        private MultiWcfGame wcfGame;










        // ####################################################
        // ##############    VISIBILITE PANEL    ##############
        // ####################################################

        /// <summary>
        /// Change la visibilité des pannels
        /// </summary>
        private void switchVisibility()
        {
            switchStatut();
            exit.Visible = false; panLogin.Visible = false; panLogout.Visible = false; panPartyCreate.Visible = false; panPartyWait.Visible = false; panScores.Visible = false; panReponses.Visible = false;
            switch (statut)
            {
                case Statut.Login: panLogin.Visible = true; break;
                case Statut.Create:
                    if (partyRadioButton.Items.Count == 0) loadParty();
                    panLogout.Visible = true; panPartyCreate.Visible = true; break;
                case Statut.Wait: loadConcurrent(); panLogout.Visible = true; panPartyWait.Visible = true; break;
                case Statut.Play: display(); panReponses.Visible = true; panLogout.Visible = true; panScores.Visible = true; exit.Visible = true; break;
            }
            loadPlayer();
            this.Validate();
        }

        /// <summary>
        /// Change le statut en fonction de l'environnement
        /// </summary>
        private void switchStatut()
        {
            if (userId == -1) statut = Statut.Login;
            else if (partyId == -1) statut = Statut.Create;
            else if (wcfGame.started(partyId)) statut = Statut.Play;
            else statut = Statut.Wait;
        }




        /// <summary>
        /// Charges les joueurs en ligne
        /// </summary>
        private void loadPlayer()
        {
            // Maj players
            Dictionary<int, Player> players = wcfGame.getPlayer();
            string result = "<ul>";
            foreach (Player p in players.Values)
                result += "<li>" + p.pseudo + "</li>";
            result += "</ul>";
            playerOnline.Text = result;

            // Maj stats
            if (userId != -1)
            {
                Player player = players[userId];
                scoresTotal.Text = player.scores.ToString();
                pourcentageTotal.Text = player.percentage().ToString();
            }


            this.Validate();
        }









        // ####################################################
        // #####################    INIT    ###################
        // ####################################################
        /// <summary>
        /// Executer au chargement de la page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            loadPage();
            wcfGame = MultiWcfGame.getInstance();
            wcfGame.setNbMusic(filesNames.Count);
            switchVisibility();
        }








        // ####################################################
        // ##############    LOGIN / LOGOUT    ################
        // ####################################################
        /// <summary>
        /// Connexion de l'utilisateur
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void loginClick(object sender, EventArgs e)
        {
            userId = wcfGame.login(inPseudo.Value, inPassword.Value);
            if (userId != -1)
            {
                loadParty();
                loadPlayer();

                switchVisibility();
            }
            else Response.Write(@"<script language='javascript'>alert('Mot de passe incorrecte');</script>");
        }


        /// <summary>
        /// Deconnexion d'un utilisateur
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void logoutClick(object sender, EventArgs e)
        {
            wcfGame.disconnect(userId, partyId);
            partyId = -1;
            userId = -1;
            switchVisibility();
        }



        // ####################################################
        // ####################    PARTY    ###################
        // ####################################################
        /// <summary>
        /// Charge les parties disponibles et joignable.
        /// </summary>
        private void loadParty()
        {
            Dictionary<int, IGame> parties = wcfGame.getParty();
            partyRadioButton.Items.Clear();
            foreach (int i in parties.Keys)
            {
                IGame m; parties.TryGetValue(i, out m);
                if (!((Multi)m).started())
                {
                    ListItem l = new ListItem(i + " " + ((Multi)m).getName(), i.ToString());
                    partyRadioButton.Items.Add(l);
                }
            }
            this.Validate();
        }

        /// <summary>
        /// Créer une partie
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void createClick(object sender, EventArgs e)
        {
            partyId = wcfGame.createParty(inPartyName.Value, int.Parse(nbPlayer.Value));
            wcfGame.init(partyId, listLevel.Value);
            wcfGame.joinParty(partyId, userId);

            switchVisibility();
        }

        /// <summary>
        /// Actualise les parties disponibles
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void refreshPartyClick(object sender, EventArgs e)
        {
            loadParty();
            this.Validate();
        }

        /// <summary>
        /// Rejoins la partie selectionner.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void joinParty_CheckedChanged(object sender, EventArgs e)
        {
            partyId = int.Parse(partyRadioButton.SelectedValue);
            if (partyId != -1 && !wcfGame.joinParty(partyId, userId))
                partyId = -1;

            switchVisibility();
        }


        /// <summary>
        /// Charge la listes des concurrents de la partie.
        /// </summary>
        private void loadConcurrent()
        {
            Dictionary<int, Player> players = wcfGame.getPlayerParty(partyId);
            string result = "<ul>";
            foreach (Player p in players.Values)
                result += "<li>" + p.pseudo + "</li>";
            result += "</ul>";
            playerParty.Text = result;
            this.Validate();
        }






        // ####################################################
        // ####################    START    ###################
        // ####################################################
        /// <summary>
        /// Démarre la partie.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void startClick(object sender, EventArgs e)
        {
            wcfGame.start(partyId);
            generateTurn();

            statut = Statut.Play;
            switchVisibility();
        }

        /// <summary>
        /// Actualise les joueurs de la partie.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void partyRefreshClick(object sender, EventArgs e)
        {
            loadConcurrent();
            this.Validate();
        }

        /// <summary>
        /// Quitte la partie et ramene au menu de selection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void exitPartyClick(object sender, EventArgs e)
        {
            partyId = -1;
            responseRadioButton.Items.Clear();
            wcfGame.resetPlayer(userId);
            switchVisibility();
        }



        // ####################################################
        // #####################    GAME    ###################
        // ####################################################
        /// <summary>
        /// Gere l'affichage
        /// </summary>
        private void display()
        {
            Dictionary<int, Player> players = wcfGame.getPlayerParty(partyId);
            string pseudo = players[userId].pseudo;
            string result = "<ul>";
            foreach (Player p in players.Values)
                if (p.pseudo != pseudo)
                    result += "<li>" + p.pseudo + " : " + p.tmpGood + " / " + p.tmpTest + "</li>";
                else
                {
                    scoreValue.Text = p.tmpGood.ToString();
                    essaiValue.Text = p.tmpTest.ToString();
                }
            result += "</ul>";
            scoreConcurent.Text = result;
            this.Validate();
        }

        /// <summary>
        /// Affiche les choix possible
        /// </summary>
        private void writeChoice()
        {
            IGame game = wcfGame.game(partyId);
            int[] result = game.generateChoice(filesNames.Count);
            responseRadioButton.Items.Clear();

            for (int i = 0; i < result.Length; i++)
                responseRadioButton.Items.Add(filesNames[result[i]]);
            this.Validate();
        }

        /// <summary>
        /// Genere un tour
        /// </summary>
        private void generateTurn()
        {
            /* Affichage */
            display();
            writeChoice();

            /* Lecture. */
            musicPlayer.Src = "/music/" + filesNames[wcfGame.game(partyId).getMusicPlayed()] + ".mp3";
        }

        /// <summary>
        /// Joue un tour.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void response_CheckedChanged(object sender, EventArgs e)
        {
            wcfGame.play(partyId, userId, responseRadioButton.SelectedIndex);
            if (!wcfGame.finish(partyId, userId))
                generateTurn();
            else
            {
                titleResponse.InnerText = "FINI";
                divResponse.InnerText = "";
                display();
            }
        }

        /// <summary>
        /// Actualise les scores des concurrents
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void refreshScoreConcurentClick(object sender, EventArgs e)
        {
            display();
            this.Validate();
        }
    }
}
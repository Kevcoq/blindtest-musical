﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GameLibrary;

namespace UnitTestBlindTest
{
    [TestClass]
    public class UnitTestGame
    {
        [TestMethod]
        public void TestMethodCreateEasy()
        {
            Game game = new Game();
            game.init(Level.Easy, 20);

            Assert.AreEqual(2, game.getNumberChoice());
        }

        [TestMethod]
        public void TestMethodCreateAverage()
        {
            Game game = new Game();
            game.init(Level.Average, 20);

            Assert.AreEqual(3, game.getNumberChoice());
        }

        [TestMethod]
        public void TestMethodCreateChallenging()
        {
            Game game = new Game();
            game.init(Level.Challenging, 20);

            Assert.AreEqual(5, game.getNumberChoice());
        }

        [TestMethod]
        public void TestMethodCreateExtreme()
        {
            Game game = new Game();
            game.init(Level.Extreme, 20);

            Assert.AreEqual(10, game.getNumberChoice());
        }

        [TestMethod]
        public void TestMethodExistChoice()
        {
            Game game = new Game();
            game.init(Level.Extreme, 20);
            int[] generate = game.generateChoice(20);
            int goodChoice = game.getAnswerPosition();

            Assert.IsTrue(game.generateChoice(20)[game.getAnswerPosition()].Equals(game.getMusicPlayed()));
        }
    }
}

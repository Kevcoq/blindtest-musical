﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GameLibrary;

namespace UnitTestBlindTest
{
    [TestClass]
    public class UnitTestPlayer
    {
        [TestMethod]
        public void TestMethodCreate()
        {
            Player player = PlayerRepository.create("test", "test");
            Assert.AreNotEqual(null, player);
        }

        [TestMethod]
        public void TestMethodCreateIncreaseFalse()
        {
            Player player = PlayerRepository.create("test", "test");
            int nbTest = player.nbTest, tmpTest = player.tmpTest, nbGood = player.goodAnswer, tmpGood = player.tmpGood, scores = player.scores;
            PlayerRepository.increase(player, false, Level.Easy);

            Assert.AreEqual(nbGood, player.goodAnswer); Assert.AreEqual(tmpGood, player.tmpGood);
            Assert.AreEqual(nbTest, player.nbTest - 1); Assert.AreEqual(tmpTest, player.tmpTest - 1);
            Assert.AreEqual(scores, player.scores);
        }

        [TestMethod]
        public void TestMethodCreateIncreaseTrueEasy()
        {
            Player player = PlayerRepository.create("test", "test");
            int nbTest = player.nbTest, tmpTest = player.tmpTest, nbGood = player.goodAnswer, tmpGood = player.tmpGood, scores = player.scores;
            PlayerRepository.increase(player, true, Level.Easy);

            Assert.AreEqual(nbGood, player.goodAnswer - 1); Assert.AreEqual(tmpGood, player.tmpGood - 1);
            Assert.AreEqual(nbTest, player.nbTest - 1); Assert.AreEqual(tmpTest, player.tmpTest - 1);
            Assert.AreEqual(scores, player.scores - 1);
        }

        [TestMethod]
        public void TestMethodCreateIncreaseTrueAverage()
        {
            Player player = PlayerRepository.create("test", "test");
            int nbTest = player.nbTest, tmpTest = player.tmpTest, nbGood = player.goodAnswer, tmpGood = player.tmpGood, scores = player.scores;
            PlayerRepository.increase(player, true, Level.Average);

            Assert.AreEqual(nbGood, player.goodAnswer - 1); Assert.AreEqual(tmpGood, player.tmpGood - 1);
            Assert.AreEqual(nbTest, player.nbTest - 1); Assert.AreEqual(tmpTest, player.tmpTest - 1);
            Assert.AreEqual(scores, player.scores - 3);
        }

        [TestMethod]
        public void TestMethodCreateIncreaseTrueChallenging()
        {
            Player player = PlayerRepository.create("test", "test");
            int nbTest = player.nbTest, tmpTest = player.tmpTest, nbGood = player.goodAnswer, tmpGood = player.tmpGood, scores = player.scores;
            PlayerRepository.increase(player, true, Level.Challenging);

            Assert.AreEqual(nbGood, player.goodAnswer - 1); Assert.AreEqual(tmpGood, player.tmpGood - 1);
            Assert.AreEqual(nbTest, player.nbTest - 1); Assert.AreEqual(tmpTest, player.tmpTest - 1);
            Assert.AreEqual(scores, player.scores - 5);
        }

        [TestMethod]
        public void TestMethodCreateIncreaseTrueExtreme()
        {
            Player player = PlayerRepository.create("test", "test");
            int nbTest = player.nbTest, tmpTest = player.tmpTest, nbGood = player.goodAnswer, tmpGood = player.tmpGood, scores = player.scores;
            PlayerRepository.increase(player, true, Level.Extreme);

            Assert.AreEqual(nbGood, player.goodAnswer - 1); Assert.AreEqual(tmpGood, player.tmpGood - 1);
            Assert.AreEqual(nbTest, player.nbTest - 1); Assert.AreEqual(tmpTest, player.tmpTest - 1);
            Assert.AreEqual(scores, player.scores - 10);
        }


        [TestMethod]
        public void TestMethodCreateIncreaseSave()
        {
            Player player = PlayerRepository.create("test", "test");
            int nbTest = player.nbTest, tmpTest = player.tmpTest, nbGood = player.goodAnswer, tmpGood = player.tmpGood, scores = player.scores;
            PlayerRepository.increase(player, true, Level.Extreme);
            PlayerRepository.save(player);

            player = PlayerRepository.login("test", "test");
            Assert.AreEqual(nbGood, player.goodAnswer - 1); Assert.AreEqual(tmpGood, player.tmpGood - 1);
            Assert.AreEqual(nbTest, player.nbTest - 1); Assert.AreEqual(tmpTest, player.tmpTest - 1);
            Assert.AreEqual(scores, player.scores - 10);
        }
    }
}

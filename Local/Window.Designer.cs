﻿namespace Local
{
    partial class Window
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Window));
            this.labelTitle = new System.Windows.Forms.Label();
            this.statsValue = new System.Windows.Forms.Label();
            this.statsName = new System.Windows.Forms.Label();
            this.groupResponses = new System.Windows.Forms.GroupBox();
            this.pauseButton = new System.Windows.Forms.Button();
            this.pourcentageValue = new System.Windows.Forms.Label();
            this.pourcentageName = new System.Windows.Forms.Label();
            this.panScores = new System.Windows.Forms.Panel();
            this.scoresValue = new System.Windows.Forms.Label();
            this.scoresName = new System.Windows.Forms.Label();
            this.bReset = new System.Windows.Forms.Button();
            this.panLevel = new System.Windows.Forms.Panel();
            this.bExtreme = new System.Windows.Forms.Button();
            this.bDifficile = new System.Windows.Forms.Button();
            this.bMoyen = new System.Windows.Forms.Button();
            this.bFacile = new System.Windows.Forms.Button();
            this.panScores.SuspendLayout();
            this.panLevel.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.BackColor = System.Drawing.Color.Transparent;
            this.labelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitle.ForeColor = System.Drawing.Color.Snow;
            this.labelTitle.Location = new System.Drawing.Point(156, 8);
            this.labelTitle.Margin = new System.Windows.Forms.Padding(3, 0, 3, 5);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(140, 17);
            this.labelTitle.TabIndex = 1;
            this.labelTitle.Text = "Blind Test Musical";
            this.labelTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // statsValue
            // 
            this.statsValue.AutoSize = true;
            this.statsValue.Location = new System.Drawing.Point(122, 34);
            this.statsValue.Name = "statsValue";
            this.statsValue.Size = new System.Drawing.Size(14, 15);
            this.statsValue.TabIndex = 3;
            this.statsValue.Text = "0";
            // 
            // statsName
            // 
            this.statsName.AutoSize = true;
            this.statsName.Location = new System.Drawing.Point(16, 34);
            this.statsName.Name = "statsName";
            this.statsName.Size = new System.Drawing.Size(89, 15);
            this.statsName.TabIndex = 2;
            this.statsName.Text = "Partie en cours";
            // 
            // groupResponses
            // 
            this.groupResponses.BackColor = System.Drawing.Color.Transparent;
            this.groupResponses.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupResponses.Location = new System.Drawing.Point(12, 36);
            this.groupResponses.Name = "groupResponses";
            this.groupResponses.Size = new System.Drawing.Size(224, 65);
            this.groupResponses.TabIndex = 6;
            this.groupResponses.TabStop = false;
            this.groupResponses.Text = "Réponses";
            this.groupResponses.Visible = false;
            // 
            // pauseButton
            // 
            this.pauseButton.Location = new System.Drawing.Point(45, 91);
            this.pauseButton.Name = "pauseButton";
            this.pauseButton.Size = new System.Drawing.Size(75, 23);
            this.pauseButton.TabIndex = 7;
            this.pauseButton.Text = "Pause";
            this.pauseButton.UseVisualStyleBackColor = true;
            this.pauseButton.Click += new System.EventHandler(this.pauseGame);
            // 
            // pourcentageValue
            // 
            this.pourcentageValue.AutoSize = true;
            this.pourcentageValue.Location = new System.Drawing.Point(122, 54);
            this.pourcentageValue.Name = "pourcentageValue";
            this.pourcentageValue.Size = new System.Drawing.Size(18, 15);
            this.pourcentageValue.TabIndex = 9;
            this.pourcentageValue.Text = "%";
            // 
            // pourcentageName
            // 
            this.pourcentageName.AutoSize = true;
            this.pourcentageName.Location = new System.Drawing.Point(16, 54);
            this.pourcentageName.Name = "pourcentageName";
            this.pourcentageName.Size = new System.Drawing.Size(77, 15);
            this.pourcentageName.TabIndex = 8;
            this.pourcentageName.Text = "Pourcentage";
            // 
            // panScores
            // 
            this.panScores.BackColor = System.Drawing.Color.Transparent;
            this.panScores.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panScores.Controls.Add(this.scoresValue);
            this.panScores.Controls.Add(this.scoresName);
            this.panScores.Controls.Add(this.bReset);
            this.panScores.Controls.Add(this.statsName);
            this.panScores.Controls.Add(this.pauseButton);
            this.panScores.Controls.Add(this.pourcentageValue);
            this.panScores.Controls.Add(this.statsValue);
            this.panScores.Controls.Add(this.pourcentageName);
            this.panScores.Location = new System.Drawing.Point(251, 36);
            this.panScores.Name = "panScores";
            this.panScores.Size = new System.Drawing.Size(157, 127);
            this.panScores.TabIndex = 10;
            this.panScores.Visible = false;
            // 
            // scoresValue
            // 
            this.scoresValue.AutoSize = true;
            this.scoresValue.Location = new System.Drawing.Point(122, 73);
            this.scoresValue.Name = "scoresValue";
            this.scoresValue.Size = new System.Drawing.Size(14, 15);
            this.scoresValue.TabIndex = 12;
            this.scoresValue.Text = "0";
            // 
            // scoresName
            // 
            this.scoresName.AutoSize = true;
            this.scoresName.Location = new System.Drawing.Point(16, 73);
            this.scoresName.Name = "scoresName";
            this.scoresName.Size = new System.Drawing.Size(45, 15);
            this.scoresName.TabIndex = 11;
            this.scoresName.Text = "Scores";
            // 
            // bReset
            // 
            this.bReset.Location = new System.Drawing.Point(45, 3);
            this.bReset.Name = "bReset";
            this.bReset.Size = new System.Drawing.Size(75, 23);
            this.bReset.TabIndex = 10;
            this.bReset.Text = "Reset";
            this.bReset.UseVisualStyleBackColor = true;
            this.bReset.Click += new System.EventHandler(this.bResetClick);
            // 
            // panLevel
            // 
            this.panLevel.BackColor = System.Drawing.Color.Transparent;
            this.panLevel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panLevel.Controls.Add(this.bExtreme);
            this.panLevel.Controls.Add(this.bDifficile);
            this.panLevel.Controls.Add(this.bMoyen);
            this.panLevel.Controls.Add(this.bFacile);
            this.panLevel.Location = new System.Drawing.Point(163, 36);
            this.panLevel.Name = "panLevel";
            this.panLevel.Size = new System.Drawing.Size(98, 106);
            this.panLevel.TabIndex = 11;
            // 
            // bExtreme
            // 
            this.bExtreme.Location = new System.Drawing.Point(9, 77);
            this.bExtreme.Name = "bExtreme";
            this.bExtreme.Size = new System.Drawing.Size(75, 23);
            this.bExtreme.TabIndex = 3;
            this.bExtreme.Text = "Extreme";
            this.bExtreme.UseVisualStyleBackColor = true;
            this.bExtreme.Click += new System.EventHandler(this.bLevelClick);
            // 
            // bDifficile
            // 
            this.bDifficile.Location = new System.Drawing.Point(9, 54);
            this.bDifficile.Name = "bDifficile";
            this.bDifficile.Size = new System.Drawing.Size(75, 23);
            this.bDifficile.TabIndex = 2;
            this.bDifficile.Text = "Difficile";
            this.bDifficile.UseVisualStyleBackColor = true;
            this.bDifficile.Click += new System.EventHandler(this.bLevelClick);
            // 
            // bMoyen
            // 
            this.bMoyen.Location = new System.Drawing.Point(9, 31);
            this.bMoyen.Name = "bMoyen";
            this.bMoyen.Size = new System.Drawing.Size(75, 23);
            this.bMoyen.TabIndex = 1;
            this.bMoyen.Tag = "1";
            this.bMoyen.Text = "Moyen";
            this.bMoyen.UseVisualStyleBackColor = true;
            this.bMoyen.Click += new System.EventHandler(this.bLevelClick);
            // 
            // bFacile
            // 
            this.bFacile.Location = new System.Drawing.Point(9, 8);
            this.bFacile.Name = "bFacile";
            this.bFacile.Size = new System.Drawing.Size(75, 23);
            this.bFacile.TabIndex = 0;
            this.bFacile.Tag = "0";
            this.bFacile.Text = "Facile";
            this.bFacile.UseVisualStyleBackColor = true;
            this.bFacile.Click += new System.EventHandler(this.bLevelClick);
            // 
            // Window
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(417, 175);
            this.Controls.Add(this.panLevel);
            this.Controls.Add(this.panScores);
            this.Controls.Add(this.groupResponses);
            this.Controls.Add(this.labelTitle);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.Name = "Window";
            this.Text = "BlindTest";
            this.panScores.ResumeLayout(false);
            this.panScores.PerformLayout();
            this.panLevel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Label statsValue;
        private System.Windows.Forms.Label statsName;
        private System.Windows.Forms.GroupBox groupResponses;
        private System.Windows.Forms.Button pauseButton;
        private System.Windows.Forms.Label pourcentageValue;
        private System.Windows.Forms.Label pourcentageName;
        private System.Windows.Forms.Panel panScores;
        private System.Windows.Forms.Panel panLevel;
        private System.Windows.Forms.Button bExtreme;
        private System.Windows.Forms.Button bDifficile;
        private System.Windows.Forms.Button bMoyen;
        private System.Windows.Forms.Button bFacile;
        private System.Windows.Forms.Button bReset;
        private System.Windows.Forms.Label scoresValue;
        private System.Windows.Forms.Label scoresName;

    }
}
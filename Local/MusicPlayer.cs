﻿using System;
using System.Text;
using System.Runtime.InteropServices;

namespace GameLibrary
{
    /// <summary>
    /// Play MP3 files
    /// </summary>
    public class MusicPlayer
    {
        private string _command;
        private bool isOpen;
        private bool isPaused = false;
        [DllImport("winmm.dll")]
        private static extern long mciSendString(string strCommand, StringBuilder strReturn, int iReturnLength, IntPtr hwndCallback);

        public MusicPlayer()
        {

        }
        public void Close()
        {
            _command = "close MediaFile";
            mciSendString(_command, null, 0, IntPtr.Zero);
            isOpen = false;
        }

        public void Open(string sFileName)
        {
            _command = "open \"" + sFileName + "\" type mpegvideo alias MediaFile";
            mciSendString(_command, null, 0, IntPtr.Zero);
            isOpen = true;
        }

        public void Play(bool loop)
        {
            if (isOpen)
            {
                _command = "play MediaFile";
                if (loop)
                    _command += " REPEAT";
                mciSendString(_command, null, 0, IntPtr.Zero);
            }
        }

        public void Pause()
        {
            if (isOpen && !isPaused)
            {
                _command = "pause MediaFile";
                mciSendString(_command, null, 0, IntPtr.Zero);
                isPaused = true;
            }
        }

        public void Resume()
        {
            if (isOpen && isPaused)
            {
                _command = "resume MediaFile";
                mciSendString(_command, null, 0, IntPtr.Zero);
                isPaused = false;
            }
        }
    }
}

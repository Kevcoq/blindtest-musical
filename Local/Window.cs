﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;
using GameLibrary;


namespace Local
{
    public partial class Window : Form
    {
        // Répertoire des mp3
        string dirName = Environment.GetFolderPath(Environment.SpecialFolder.MyMusic) + "\\";

        // Random
        Random rand = new Random();

        // Le jeux
        Solo game;
        Player player = null;

        // Le player pour les mp3
        MusicPlayer musicPlayer = new MusicPlayer();
        bool pause = false;

        // Liste des fichiers
        List<string> filesNames = new List<string>();

        // Listes des choix possibles
        List<RadioButton> radioButtons = new List<RadioButton>();

        public Window()
        {
            /* Ajout des fichiers mp3 au tableau de nom. */
            char[] removeMP3 = new char[] { '.', 'm', 'p', '3' };
            DirectoryInfo fileListing = new DirectoryInfo(dirName);
            foreach (FileInfo file in fileListing.GetFiles("*.mp3"))
            {
                filesNames.Add(file.Name.TrimEnd(removeMP3));
            }

            InitializeComponent();
            panScores.BackColor = Color.FromArgb(75, Color.White);
            groupResponses.BackColor = Color.FromArgb(75, Color.White);

            player = PlayerRepository.deserialize();
            if (player == null)
                player = PlayerRepository.create("LocalHost", "localhost");
        }

        /// <summary>
        /// Affiche le score
        /// </summary>
        private void display()
        {
            statsValue.Text = player.tmpGood.ToString() + " / " + player.tmpTest.ToString();
            pourcentageValue.Text = player.percentage().ToString() + "%";
            scoresValue.Text = player.scores.ToString();
        }

        /// <summary>
        /// Génére le jeu et attend la réponse pour calculer le score puis générer le tour suivant.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void responseChoose(object sender, EventArgs e)
        {
            /* Détection du mode pause. */
            if (pause) { return; }

            /* Ferme le lecteur */
            musicPlayer.Close();

            /* Affiche en couleur la réponse vert = OK | rouge = KO */
            radioButtons[game.getAnswerPosition()].ForeColor = game.play(0, radioButtons.IndexOf((RadioButton)sender)) ? Color.Green : Color.Red;
            this.Refresh();
            Thread.Sleep(500);
            radioButtons[game.getAnswerPosition()].ForeColor = Color.Black;
            ((RadioButton)sender).Checked = false;

            /* Génération du tour suivant */
            generateTurn();
        }

        /// <summary>
        /// Genere un tour
        /// </summary>
        private void generateTurn()
        {
            /* Affichage */
            display();
            writeChoice();

            /* Lecture. */
            musicPlayer.Open(dirName + filesNames[game.getMusicPlayed()] + ".mp3");
            musicPlayer.Play(false);
        }

        /// <summary>
        /// Inscrit par bouton un titre aléatoire unique.
        /// </summary>
        private void writeChoice()
        {
            int[] result = game.generateChoice(filesNames.Count);
            for (int i = 0; i < result.Length; i++)
                radioButtons[i].Text = filesNames[result[i]];
        }

        /// <summary>
        /// permet de mettre le jeu en pause
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pauseGame(object sender, EventArgs e)
        {
            if (pause) { musicPlayer.Resume(); pauseButton.Text = "Pause"; }
            else { musicPlayer.Pause(); pauseButton.Text = "Reprendre"; }
            pause = !pause;
        }

        /// <summary>
        /// Choisi le niveau au démmarage du jeu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bLevelClick(object sender, EventArgs e)
        {
            // Change visibility
            panLevel.Visible = false;
            panScores.Visible = true;
            groupResponses.Visible = true;

            // reset Player tmp
            player.tmpGood = 0;
            player.tmpTest = 0;

            // reset game
            game = new Solo(player);
            game.init(LevelExtension.stringFrToLevel(((Button)sender).Text), filesNames.Count);

            groupResponses.Controls.Clear();
            radioButtons.Clear();
            int xButton = 7, yButton = 25;
            for (int i = 0; i < game.getNumberChoice(); i++)
            {
                RadioButton radio = new RadioButton();
                radio.Click += responseChoose;
                radio.Location = new Point(xButton, yButton);
                radio.Width = 250;
                yButton += 25;
                radioButtons.Add(radio);
                groupResponses.Controls.Add(radio);
            }
            this.Height = yButton + 120;
            groupResponses.Height = yButton;
            generateTurn();
        }

        /// <summary>
        /// Affiche le menu d'accueil
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bResetClick(object sender, EventArgs e)
        {
            panLevel.Visible = true;
            panScores.Visible = false;
            groupResponses.Visible = false;
            musicPlayer.Close();
            this.Height = 200;
        }

        /// <summary>
        /// Save the user with serializer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);

            if (e.CloseReason == CloseReason.WindowsShutDown) return;
            PlayerRepository.serialize(player);
        }
    }
}

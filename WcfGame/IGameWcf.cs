﻿using GameLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WcfGame
{
    [ServiceContract]
    public interface IGameWcf
    {
        [OperationContract]
        void setNbMusic(int numberMusic);

        [OperationContract]
        int login(string pseudo, string password);

        [OperationContract]
        void init(int id, string level);

        [OperationContract]
        void disconnect(int playerId, int partyId);

        [OperationContract]
        void resetPlayer(int playerId);

        [OperationContract]
        IGame game(int partyId);

        [OperationContract]
        Player player(int playerId);
    }
}

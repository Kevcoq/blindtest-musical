﻿using GameLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfGame
{
    public class CommunWCF : IGameWcf
    {
        // Les parties
        protected static Dictionary<int, IGame> games = new Dictionary<int, IGame>();
        protected static Dictionary<int, Player> players = new Dictionary<int, Player>();
        protected static int idsParty = 0;
        protected static int idsPlayer = 0;

        protected int numberMusic;

        public void setNbMusic(int numberMusic) { this.numberMusic = numberMusic; }

        public void init(int id, string level)
        {
            IGame game; games.TryGetValue(id, out game);
            game.init(LevelExtension.stringFrToLevel(level), numberMusic);
        }

        public int login(string pseudo, string password)
        {
            Player player = PlayerRepository.login(pseudo, password); if (player != null)
            {
                players.Add(idsPlayer, player);
                return idsPlayer++;
            }
            else return -1;
        }

        public void disconnect(int playerId, int partyId)
        {
            Player player = this.player(playerId);
            PlayerRepository.save(player);
            players.Remove(playerId);
            if (this is SoloWcfGame) games.Remove(partyId);
            else
            {
                if (partyId != -1)
                {
                    IGame game = this.game(partyId);
                    ((Multi)game).removePlayer(playerId);
                }
            }
        }

        public void resetPlayer(int playerId)
        {
            Player player = this.player(playerId);
            player.tmpGood = 0;
            player.tmpTest = 0;
            PlayerRepository.save(player);
        }

        public IGame game(int partyId) { IGame game; games.TryGetValue(partyId, out game); return game; }
        public Player player(int playerId) { Player player; players.TryGetValue(playerId, out player); return player; }
    }
}
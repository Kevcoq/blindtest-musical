﻿using GameLibrary;
using System;
using System.Collections.Generic;

namespace WcfGame
{
    public class SoloWcfGame : CommunWCF, ISoloWcf
    {
        private static SoloWcfGame instance = null;
        private SoloWcfGame() { }

        public static SoloWcfGame getInstance()
        {
            if (instance == null)
                instance = new SoloWcfGame();
            return instance;
        }

        public int createParty(int playerId) { Player player; players.TryGetValue(playerId, out player); games.Add(idsParty, new Solo(player)); return idsParty++; }

        public bool play(int id, int response)
        {
            IGame game; games.TryGetValue(id, out game);
            return game.play(0, response);
        }
    }
}

﻿using GameLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WcfGame
{
    [ServiceContract]
    public interface IMultiWcf : IGameWcf
    {
        [OperationContract]
        int createParty(string partyName, int maxPlayer);

        [OperationContract]
        void start(int partyId);

        [OperationContract]
        bool started(int partyId);

        [OperationContract]
        Dictionary<int, IGame> getParty();

        [OperationContract]
        Dictionary<int, Player> getPlayer();

        [OperationContract]
        Dictionary<int, Player> getPlayerParty(int partyId);

        [OperationContract]
        bool joinParty(int partyId, int playerId);

        [OperationContract]
        bool play(int idParty, int idPlayer, int response);


    }
}

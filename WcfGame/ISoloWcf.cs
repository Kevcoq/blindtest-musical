﻿using GameLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfGame
{
    [ServiceContract]
    public interface ISoloWcf : IGameWcf
    {
        [OperationContract]
        int createParty(int playerId);

        [OperationContract]
        bool play(int id, int response);
    }
}

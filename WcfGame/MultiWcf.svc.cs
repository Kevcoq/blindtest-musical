﻿using GameLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfGame
{
    public class MultiWcfGame : CommunWCF, IMultiWcf
    {
        private static MultiWcfGame instance = null;
        private MultiWcfGame() { }

        public static MultiWcfGame getInstance()
        {
            if (instance == null)
                instance = new MultiWcfGame();
            return instance;
        }

        public int createParty(string partyName, int maxPlayer) { games.Add(idsParty, new Multi(partyName, maxPlayer)); return idsParty++; }
        public Dictionary<int, IGame> getParty() { return games; }
        public Dictionary<int, Player> getPlayer() { return players; }

        public bool joinParty(int partyId, int playerId)
        {
            IGame game; games.TryGetValue(partyId, out game); Player player; players.TryGetValue(playerId, out player);

            if (game == null || player == null)
            {
                if (game == null) { throw new Exception("Partie non trouvé : " + partyId); }
                else { throw new Exception("Joueur non trouvé : " + playerId); }
            }
            return ((Multi)game).joinParty(playerId, player);
        }

        public Dictionary<int, Player> getPlayerParty(int partyId) { IGame game; games.TryGetValue(partyId, out game); return ((Multi)game).getPlayer(); }

        public void start(int partyId) { IGame game; games.TryGetValue(partyId, out game); ((Multi)game).start(); }
        public bool started(int partyId) { IGame game; games.TryGetValue(partyId, out game); return ((Multi)game).started(); }

        public bool play(int partyId, int playerId, int response) { IGame game; games.TryGetValue(partyId, out game); return game.play(playerId, response); }


        public bool finish(int partyId, int playerId) { IGame game; games.TryGetValue(partyId, out game); return ((Multi)game).finish(playerId); }
    }
}

﻿using System;
using System.Collections.Generic;

namespace GameLibrary
{
    /// <summary>
    /// La classe contenant le jeux
    /// </summary>
    public class Game : IGame
    {
        int AnswerReponse { get; set; }
        int MusicPlayed { get; set; }
        int NumberMusic { get; set; }
        int NumberChoice { get; set; }

        private Level level;

        // Random
        private Random rand;

        public void init(Level level, int numberMusic)
        {
            rand = new Random();

            AnswerReponse = -1;
            NumberMusic = numberMusic;

            this.level = level;
            NumberChoice = (int)level;
            if (NumberChoice > NumberMusic) { NumberChoice = NumberMusic; }
        }

        public bool play(int id, int response) { return response == AnswerReponse; }

        public int[] generateChoice(int numberMusic)
        {
            AnswerReponse = rand.Next(NumberChoice);
            MusicPlayed = rand.Next(numberMusic);

            int[] result = new int[NumberChoice];
            List<int> titleAdded = new List<int>();
            titleAdded.Add(MusicPlayed);
            for (int i = 0; i < NumberChoice; i++)
            {
                int song;
                if (i == AnswerReponse)
                    song = MusicPlayed;
                else
                {
                    int tmp = rand.Next(numberMusic);
                    while (titleAdded.Contains(tmp))
                        tmp = rand.Next(numberMusic);
                    titleAdded.Add(tmp);
                    song = tmp;
                }

                // Ajout de la chanson
                result[i] = song;
            }
            return result;
        }

        public int getMusicPlayed() { return MusicPlayed; }
        public int getAnswerPosition() { return AnswerReponse; }
        public int getNumberChoice() { return NumberChoice; }

        public Level getLevel() { return level; }
    }
}

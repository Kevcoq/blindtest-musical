﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLibrary
{
    public class Multi : GameDecorator
    {
        /* Jeux */
        private Dictionary<int, Player> players;
        private int maxPlayer;
        private string name;
        private bool go = false;

        public string getName() { return name; }


        public Multi(string name, int maxPlayer)
        {
            this.name = name;
            this.maxPlayer = maxPlayer;
        }

        public override void init(Level level, int numberMusic)
        {
            this.players = new Dictionary<int, Player>();
            this.game = new Game();
            this.game.init(level, numberMusic);
        }

        public bool joinParty(int id, Player player)
        {
            bool returnValue = false;
            if (!go && players.Count < maxPlayer) { players.Add(id, player); returnValue = true; }
            return returnValue;
        }

        public void start() { go = true; }

        public bool started() { return go; }

        public override bool play(int id, int response)
        {
            Player player;
            players.TryGetValue(id, out player);

            if (player == null)
                throw new Exception("Le joueur n'existe pas.");
            bool returnValue = game.play(id, response);

            PlayerRepository.increase(player, returnValue, game.getLevel());
            return returnValue;
        }

        public Dictionary<int, Player> getPlayer() { return players; }

        public void removePlayer(int playerId) { players.Remove(playerId); }

        public bool finish(int id)
        {
            bool result = true;
            Player player;
            players.TryGetValue(id, out player);
            if (player != null && player.tmpTest < 10)
                result = false;
            return result;
        }
    }
}

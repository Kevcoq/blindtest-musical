﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLibrary
{
    public enum Level : int
    {
        Easy = 2,
        Average = 3,
        Challenging = 5,
        Extreme = 10
    }

    public static class LevelExtension
    {


        public static Level stringToLevel(string text)
        {
            Level level;
            switch (text)
            {
                case "Easy": level = Level.Easy; break;
                case "Challenging": level = Level.Challenging; break;
                case "Extreme": level = Level.Extreme; break;
                default: level = Level.Average; break;
            }
            return level;
        }

        public static Level stringFrToLevel(string text)
        {
            Level level;
            switch (text)
            {
                case "Facile": level = Level.Easy; break;
                case "Difficile": level = Level.Challenging; break;
                case "Extreme": level = Level.Extreme; break;
                default: level = Level.Average; break;
            }
            return level;
        }
    }
}

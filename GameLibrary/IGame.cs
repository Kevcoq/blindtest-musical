﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLibrary
{
    public interface IGame
    {
        /// <summary>
        /// Constructeur complexe
        /// </summary>
        /// <param name="level">Le niveau du jeu</param>
        /// <param name="numberMusic">Le nombre de musique</param>
        void init(Level level, int numberMusic);

        /// <summary>
        /// Vérifie la réponse
        /// </summary>
        /// <param name="id">l'id du joueur, obselete en partie solo</param>
        /// <param name="response">la réponse</param>
        /// <returns>vrai si la réponse est juste.</returns>
        bool play(int id, int response);

        /// <summary>
        /// Genere les choix
        /// </summary>
        /// <param name="numberMusic">le nombre de musique</param>
        /// <returns></returns>
        int[] generateChoice(int numberMusic);

        int getMusicPlayed();
        int getAnswerPosition();
        int getNumberChoice();

        Level getLevel();
    }
}

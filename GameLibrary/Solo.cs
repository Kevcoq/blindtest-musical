﻿
namespace GameLibrary
{
    public class Solo : GameDecorator
    {
        private Player player;
        public Solo(Player player) { this.player = player; }

        public override void init(Level level, int numberMusic) { game = new Game(); game.init(level, numberMusic); }

        public override bool play(int id, int response)
        {
            bool returnValue = game.play(id, response);
            PlayerRepository.increase(player, returnValue, game.getLevel());
            return returnValue;
        }
    }
}

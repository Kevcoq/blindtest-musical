﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GameLibrary
{
    public static class PlayerRepository
    {
        public static Player create(string pseudo, string password)
        {
            Player player = new Player();
            player.pseudo = pseudo;
            player.password = password;
            return player;
        }

        public static Player login(string pseudo, string password)
        {
            Player player = null;
            using (BlindTestEntities1 context = new BlindTestEntities1())
            {
                var query = from p in context.Player where p.pseudo == pseudo select p;
                List<Player> allPlayer = query.ToList();

                if (allPlayer.Count > 0)
                {
                    player = allPlayer[0];
                    if (player.password != password)
                        player = null;
                }
                else
                {
                    player = create(pseudo, password);
                    context.Player.Add(player);
                    context.SaveChanges();
                }
            }
            if (player != null)
            {
                player.tmpGood = 0;
                player.tmpTest = 0;
            }
            return player;
        }

        public static void increase(Player p, bool result, Level level)
        {
            int addScore = 0;
            if (result)
            {
                p.goodAnswer++; p.tmpGood++;
                switch (level)
                {
                    case Level.Easy: addScore = 1; break;
                    case Level.Average: addScore = 3; break;
                    case Level.Challenging: addScore = 5; break;
                    case Level.Extreme: addScore = 10; break;
                }
            }

            p.nbTest++; p.tmpTest++;
            p.scores += addScore;
        }

        public static void save(Player player)
        {
            using (BlindTestEntities1 context = new BlindTestEntities1())
            {
                context.Player.Attach(player);
                context.Entry(player).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
        }

        public static void delete(Player player)
        {
            using (BlindTestEntities1 context = new BlindTestEntities1())
            {
                context.Player.Remove(player);
                context.SaveChanges();
            }
        }

        public static void serialize(Player player)
        {
            XmlSerializer xs = new XmlSerializer(typeof(Player));
            using (StreamWriter wr = new StreamWriter("player.xml"))
            {
                xs.Serialize(wr, player);
            }
        }

        public static Player deserialize()
        {
            Player p = null;
            XmlSerializer xs = new XmlSerializer(typeof(Player));
            if (File.Exists("player.xml"))
                using (StreamReader rd = new StreamReader("player.xml"))
                {
                    p = xs.Deserialize(rd) as Player;
                }
            return p;
        }
    }
}

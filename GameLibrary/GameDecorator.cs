﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLibrary
{
    public abstract class GameDecorator : IGame
    {
        protected Game game;

        public int[] generateChoice(int numberMusic) { return game.generateChoice(numberMusic); }
        public int getMusicPlayed() { return game.getMusicPlayed(); }
        public int getAnswerPosition() { return game.getAnswerPosition(); }
        public int getNumberChoice() { return game.getNumberChoice(); }
        public Level getLevel() { return game.getLevel(); }


        public abstract void init(Level level, int numberMusic);
        public abstract bool play(int id, int response);
    }
}
